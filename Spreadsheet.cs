﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO;
using System.Net;
using System.Windows;

namespace Spreadsheet_Viewer
{
    class Spreadsheet
    {
        public static bool allowDownload;
        static int rowsPerPage = 10;
        public static int RowsPerPage { get; set; }
        public int currentPage = 0;
        public int lastReadRow = 2;
        public  List<SpreadsheetLine> listOfLines = new List<SpreadsheetLine>();
        public List<List<SpreadsheetLine>> listOfPages = new List<List<SpreadsheetLine>>();
        public static List<string[]> SpreadsheetHeaders = new List<string[]>();

        
        public void ReadSpreadsheet()
        {
            lastReadRow = 2;
            Config.OverrideConfigFile();
            SpreadsheetLine.ReadTitles();
            while (!this.NextLineIsEmpty())
            {
                SpreadsheetLine.ReadLine(this);
            }
            this.SplitForPages();
        }

        public bool NextLineIsEmpty()
        {
            byte[] bin = File.ReadAllBytes($@"{Config.filepath}\{Config.filename}");
            using (MemoryStream stream = new MemoryStream(bin))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet currentWorksheet = excelPackage.Workbook.Worksheets[1];
                if (currentWorksheet.Cells[this.lastReadRow + 1, 1].Value == null)
                {
                    return true;
                }
                else { return false; }
            }
        }
        public List<List<SpreadsheetLine>> SplitForPages()
        {
            List<List<SpreadsheetLine>> pagesOfSpreadsheet = new List<List<SpreadsheetLine>>();
            for (int i=rowsPerPage; i<this.listOfLines.Count; i+=rowsPerPage)
            {
                List<SpreadsheetLine> page = new List<SpreadsheetLine>();
                for (int j=i-rowsPerPage; j<i; j++)
                {
                    page.Add(this.listOfLines[j]);
                }
                pagesOfSpreadsheet.Add(page);
            }
            if (pagesOfSpreadsheet.Count * rowsPerPage != listOfLines.Count)
            {
                List<SpreadsheetLine> page = new List<SpreadsheetLine>();
                for (int i = pagesOfSpreadsheet.Count * rowsPerPage; i < listOfLines.Count; i++)
                {
                    page.Add(this.listOfLines[i]);
                }
                pagesOfSpreadsheet.Add(page);
            }
            this.listOfPages = pagesOfSpreadsheet;
            return pagesOfSpreadsheet;
        }
        public List<SpreadsheetLine> ReassemblePages()
        {
            List<SpreadsheetLine> newListOfLines = new List<SpreadsheetLine>();
            foreach(List<SpreadsheetLine> page in this.listOfPages)
            {
                foreach(SpreadsheetLine line in page)
                {
                    newListOfLines.Add(line);
                }
            }
            return newListOfLines;
        }
        public static void SaveExcelDoc(List<SpreadsheetLine> linesToSave)
        {
            //Config.OverrideConfigFile();
            FileInfo outputFile = new FileInfo($@"{Config.filepath}\{Config.filename}");
            if (!outputFile.ToString().EndsWith(".xlsx"))
            {
                outputFile = new FileInfo($@"{Config.filepath}\{Config.filename}.xlsx");
            }
            if (outputFile.Exists)
            {
                outputFile.Delete();
            }
            using (ExcelPackage excelPackage = new ExcelPackage(outputFile))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Лист 1");
                worksheet.Cells["A1:E1"].Merge = true;
                worksheet.Cells["A1"].Value = $"{SpreadsheetHeaders[0][0].ToString()}";
                worksheet.Cells["F1:H1"].Merge = true;
                worksheet.Cells["F1"].Value = $"{SpreadsheetHeaders[0][1].ToString()}";
                worksheet.Cells["I1:J1"].Merge = true;
                worksheet.Cells["I1"].Value = $"{SpreadsheetHeaders[0][2].ToString()}";
                string alphabet = "ABCDEFGHIJ";
                int[] width = { 21, 53, 35, 46, 55, 32,26,26, 30,30};
                int[] columnAlighmentCenter = { 0, 5, 6, 7, 8, 9 };
                for (int i=0; i<alphabet.Length; i++)
                {
                    worksheet.Cells[$"{alphabet[i]}2"].Value = SpreadsheetHeaders[1][i].ToString();
                    worksheet.Column(i+1).Width = width[i];
                    if (columnAlighmentCenter.Contains(i))
                    {
                        worksheet.Column(i+1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    }
                }
                int counter = 3;
                foreach (SpreadsheetLine line in linesToSave)
                {
                    worksheet.Cells[$"A{counter}"].Value = (line.ID.ToString());
                    worksheet.Cells[$"B{counter}"].Value = (line.Name.ToString());
                    worksheet.Cells[$"C{counter}"].Value = (line.Description.ToString());
                    worksheet.Cells[$"D{counter}"].Value = (line.SourceOfThreat.ToString());
                    worksheet.Cells[$"E{counter}"].Value = (line.ThreatenObject.ToString());
                    worksheet.Cells[$"F{counter}"].Value = (line.ConfidentialityBreach ? "1":"0");
                    worksheet.Cells[$"G{counter}"].Value = (line.IntegrityBreach ? "1":"0");
                    worksheet.Cells[$"H{counter}"].Value = (line.AvailabilityBreach ? "1":"0");
                    worksheet.Cells[$"I{counter}"].Value = (line.IntroductionDate.ToString());
                    worksheet.Cells[$"J{counter}"].Value = (line.LastChangeDate.ToString());
                    counter++;
                }
                excelPackage.SaveAs(outputFile);
            }
        }
        public static void DownloadSpreadsheet(string fileURL, string fileName, string filePath)
        {
                WebClient downloader = new WebClient();
                downloader.DownloadFile($@"{fileURL}", $@"{Config.filepath}\{Config.filename}");
        }
        public static bool IsSpreadsheetAvailable(string filePath, string Filename)
        {
            try { byte[] bin = File.ReadAllBytes($@"{filePath}\{Filename}"); }
            catch (Exception e) 
            { 
                MessageBox.Show("Ошибка при попытке открыть файл. Возможно файл открыт в другой программе \n" + e.Message);
                return false;
            }
            return true;
        }
        public static List<List<SpreadsheetLine>> CheckDifferencesToOnline(Spreadsheet spreadsheet)
        {
            WebClient downloader = new WebClient();
            string currentConfigName = Config.filename;
            Config.filename = "Temp.xlsx";
            try
            {
                downloader.DownloadFile($@"{Config.fileurl}", $@"{Config.filepath}\Temp.xlsx");
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удаётся получить доступ к серверу с файлом для проверки акутальности: " + e.Message);
            }
            if (!File.Exists($@"{Config.filepath}\Temp.xlsx")) 
            { 
                Config.filename = currentConfigName;   
                return new List<List<SpreadsheetLine>>(); 
            }
            Spreadsheet LatestSpreadsheet = new Spreadsheet();
            LatestSpreadsheet.ReadSpreadsheet();
            Config.filename = currentConfigName;
            Config.OverrideConfigFile();
            List<SpreadsheetLine> missingInLocal = new List<SpreadsheetLine>();
            List<SpreadsheetLine> changedInLocal = new List<SpreadsheetLine>();
            foreach (SpreadsheetLine line in LatestSpreadsheet.listOfLines)
            {
                bool lineIsMissing = true;
                bool lineIsChanged = true;
                foreach (SpreadsheetLine alsoLine in spreadsheet.listOfLines)
                {
                    if (line.ToString() == alsoLine.ToString())
                    {
                        lineIsMissing = false;
                        lineIsChanged = false;
                        break;
                    }
                    if (line.ShortToString().Substring(4,3).Equals(alsoLine.ShortToString().Substring(4,3)))
                    {
                        lineIsMissing = false;
                        break;
                    }
                }
                if (lineIsMissing)
                {
                      missingInLocal.Add(line);
                      continue;
                      
                }
                if (lineIsChanged)
                {
                    changedInLocal.Add(line);
                }
            }
            File.Delete($@"{Config.filepath}\Temp.xlsx");
            List<List<SpreadsheetLine>> changes = new List<List<SpreadsheetLine>>();
            changes.Add(missingInLocal);
            changes.Add(changedInLocal);
            Config.filename = currentConfigName;
            return changes;
        }
    }
}
