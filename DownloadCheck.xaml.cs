﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;

namespace Spreadsheet_Viewer
{
    public partial class DownloadCheck : Window
    {
        public DownloadCheck()
        {
            InitializeComponent();
        }

        private void Button_Click_Accept(object sender, RoutedEventArgs e)
        {
            if (URLIsCorrect() & PathIsCorrect() & NameIsCorrect())
            {
                Config.fileurl = TextBox_DefaultUrl.Text;
                Config.filename = TextBox_DefaultName.Text;
                Spreadsheet.allowDownload = true;
                this.Close();
            }
        }
        private void Button_Click_Deny(object sender, RoutedEventArgs e)
        {
            Spreadsheet.allowDownload = false;
            this.Close();
        }
        private bool URLIsCorrect()
        {

            if (TextBox_DefaultUrl.Text == "")
            {
                TextBlock_DefaultUrl.Foreground = Brushes.Red;
                TextBlock_DefaultUrl.Text = "Введённое значение URL некорректно!";
                return false;
            }
            if (TextBox_DefaultUrl.Text.Substring(TextBox_DefaultUrl.Text.Length - 5, 5) != ".xlsx")
            {
                TextBlock_DefaultUrl.Foreground = Brushes.Red;
                TextBlock_DefaultUrl.Text = "Введённое значение URL не указывает на .xlsx файл";
                return false;
            }
            WebClient newClient = new WebClient();
            string response;
            try
            {
                response = newClient.DownloadString($@"{TextBox_DefaultUrl.Text}");
            }
            catch
            {
                try
                {
                    response = newClient.DownloadString(@"http://www.google.com");
                }
                catch
                {
                    TextBlock_DefaultUrl.Foreground = Brushes.Red;
                    TextBlock_DefaultUrl.Text = ("Не удаётся установить соединение с интернетом");
                    return false;
                }
                TextBlock_DefaultUrl.Foreground = Brushes.Red;
                TextBlock_DefaultUrl.Text = ("Не удаётся установить соединение с введённым URL");
                return false;
            }
            TextBlock_DefaultUrl.Foreground = Brushes.Green;
            TextBlock_DefaultUrl.Text = "Значение URL введено корректно!";
            return true;
        }
        private bool PathIsCorrect()
        {
            TextBlock_DefaultFilepath.Foreground = Brushes.Green;
            TextBlock_DefaultFilepath.Text = "Значение пути для файла введено корректно";
            return true;
        }
        private bool NameIsCorrect()
        {
            string ForbiddenSymbols = @"~#%&*{}\:<>?/+|'";
            if (TextBox_DefaultName.Text.Length == 0)
            {
                TextBlock_DefaultName.Foreground = Brushes.Red;
                TextBlock_DefaultName.Text = "Значение имени не может быть пустым!";
                return false;
            }
            else
            {
                foreach (char letter in TextBox_DefaultName.Text)
                {
                    foreach (char symbol in ForbiddenSymbols)
                    {
                        if (letter == symbol)
                        {
                            TextBlock_DefaultName.Foreground = Brushes.Red;
                            TextBlock_DefaultName.Text = $"Имя файла не может содержать символ {symbol}";
                            return false;
                        }
                    }
                }
                TextBlock_DefaultName.Foreground = Brushes.Green;
                TextBlock_DefaultName.Text = "Значение имени файла введено корректно";
            }
            if (TextBox_DefaultName.Text.Substring(TextBox_DefaultName.Text.Length-5,5)!= ".xlsx")
            {
                TextBox_DefaultName.Text = TextBox_DefaultName.Text + ".xlsx";
            }
            return true;
        }

        private void TextBox_DefaultPath_Click(object sender, RoutedEventArgs e)
        {
            FileExplorer newExplorer = new FileExplorer();
            newExplorer.TextBox_FileName.IsEnabled = false;
            newExplorer.ShowDialog();
            TextBox_DefaultPath.Content = FileExplorer.ShortenPathToFile(FileExplorer.currentFolder);
        }
    }
}
