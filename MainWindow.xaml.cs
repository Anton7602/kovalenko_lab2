﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using OfficeOpenXml;
using System.Reflection;

namespace Spreadsheet_Viewer
{

    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            Config.InitiateConfigFile();
            InitializeComponent();
            Spreadsheet mainSpreadsheet = new Spreadsheet();
            SpreadsheetView.ItemsSource = null;
            if (File.Exists($@"{Config.filepath}\{Config.filename}"))
            {
                NotFoundMessage.Text = "Идёт загрузка локального файла";
                if (Spreadsheet.IsSpreadsheetAvailable(Config.filepath, Config.filename))
                {
                    mainSpreadsheet = InitializeSpreadsheet();
                }
            }
            ///============================================================Buttons Declaration====================================================================
            ButtonNextPage.Click += (sender, args) =>
            {
                if (mainSpreadsheet!=null && mainSpreadsheet.currentPage<mainSpreadsheet.listOfPages.Count-1)
                {
                    mainSpreadsheet.currentPage++;
                    SpreadsheetView.ItemsSource = mainSpreadsheet.listOfPages[mainSpreadsheet.currentPage];
                }
            };
            ButtonPreviousPage.Click += (sender, args) =>
            {
                if (mainSpreadsheet != null && mainSpreadsheet.currentPage > 0)
                {
                    mainSpreadsheet.currentPage--;
                    SpreadsheetView.ItemsSource = mainSpreadsheet.listOfPages[mainSpreadsheet.currentPage];
                }
            };
            ButtonSaveToFile.Click += (sender, args) =>
            {
                List<SpreadsheetLine> linesToSave = new List<SpreadsheetLine>();
                linesToSave = mainSpreadsheet.ReassemblePages();
                Spreadsheet.SaveExcelDoc(linesToSave);
            };
            ButtonSaveAsFile.Click += (sender, args) =>
            {
                FileExplorer newExplorer = new FileExplorer();
                FileExplorer.isForSave = true;
                newExplorer.ShowDialog();
                if (FileExplorer.isContinueAllowed)
                {
                    List<SpreadsheetLine> linesToSave = new List<SpreadsheetLine>();
                    linesToSave = mainSpreadsheet.ReassemblePages();
                    Spreadsheet.SaveExcelDoc(linesToSave);
                }
            };
            MenuButtonDownload.Click += (sender, args) =>
            {
                DownloadCheck newCheck = new DownloadCheck();
                newCheck.Owner = this;
                newCheck.TextBox_DefaultUrl.Text = Config.fileurl;
                newCheck.TextBox_DefaultName.Text = Config.filename;
                newCheck.TextBox_DefaultPath.Content = Config.filepath;
                newCheck.TextBox_DefaultPath.Content = FileExplorer.ShortenPathToFile(Config.filepath);
                Spreadsheet.allowDownload = false;
                newCheck.ShowDialog();
                try
                {
                    if (Spreadsheet.allowDownload == true)
                    {
                        Spreadsheet.DownloadSpreadsheet(Config.fileurl, Config.filename, Config.filepath);
                        mainSpreadsheet = InitializeSpreadsheet();
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Ошибка при скачивании файла: " + e.Message);
                }
            };
            ButtonDownload.Click += (sender, args) =>
             {
                 DownloadCheck newCheck = new DownloadCheck();
                 newCheck.Owner = this;
                 newCheck.TextBox_DefaultUrl.Text = Config.fileurl;
                 newCheck.TextBox_DefaultName.Text = Config.filename;
                 newCheck.TextBox_DefaultPath.Content = Config.filepath;
                 newCheck.TextBox_DefaultPath.Content = FileExplorer.ShortenPathToFile(Config.filepath);
                 Spreadsheet.allowDownload = false;
                 newCheck.ShowDialog();
                 try
                 {
                     if (Spreadsheet.allowDownload == true)
                     {
                         Spreadsheet.DownloadSpreadsheet(Config.fileurl, Config.filename, Config.filepath);
                         mainSpreadsheet = InitializeSpreadsheet();
                     }
                 }
                 catch (Exception e)
                 {
                     MessageBox.Show("Ошибка при скачивании файла: " + e.Message);
                 }
             };
            MenuButtonOpen.Click += (sender, args) =>
            {
                FileExplorer newFileexplorer = new FileExplorer();
                newFileexplorer.ShowDialog();
                if (FileExplorer.isContinueAllowed)
                {
                    mainSpreadsheet = new Spreadsheet();
                    SpreadsheetView.ItemsSource = null;
                    Config.filename = FileExplorer.currentName;
                    Config.filepath = FileExplorer.currentFolder;
                    if (Spreadsheet.IsSpreadsheetAvailable(Config.filepath, Config.filename))
                    {
                        mainSpreadsheet = InitializeSpreadsheet();
                    }
                }
            };
            ButtonFindLocal.Click += (sender, args) =>
            {
                FileExplorer newFileexplorer = new FileExplorer();
                newFileexplorer.ShowDialog();
                if (FileExplorer.isContinueAllowed)
                {
                    Config.filename = FileExplorer.currentName;
                    Config.filepath = FileExplorer.currentFolder;
                    if (Spreadsheet.IsSpreadsheetAvailable(Config.filepath, Config.filename))
                    {
                        mainSpreadsheet = InitializeSpreadsheet();
                    }
                }
            };
            Menu_CloseFile.Click += (sender, args) =>
            {
                SpreadsheetView.ItemsSource = null;
                mainSpreadsheet = new Spreadsheet();
                ButtonsForPages.Children.Clear();
                ButtonDownload.Visibility = Visibility.Visible;
                ButtonFindLocal.Visibility = Visibility.Visible;
                MissingSpreadsheetLines.Text = "Недостающие записи в локальной копии: ";
                ChangedSpreadsheetLines.Text = "Изменённые записи в локальной копии: ";
                MissingSpreadsheetLines.Visibility = Visibility.Hidden;
                ChangedSpreadsheetLines.Visibility = Visibility.Hidden;
                ShowChangedLines.Visibility = Visibility.Hidden;
                ShowMissingLines.Visibility = Visibility.Hidden;
                DateOfLastChange.Visibility = Visibility.Hidden;
                ShowNumberOFElements.Visibility = Visibility.Hidden;
                this.Title = "Spreadsheet_Viewer";
            };
            ShowMissingLines.Click += (sender, args) =>
            {
                List<SpreadsheetLine> missingLines = Spreadsheet.CheckDifferencesToOnline(mainSpreadsheet)[0];
                ShowDifference differenceSpreadsheet = new ShowDifference();
                differenceSpreadsheet.DifferenceView.ItemsSource = missingLines;
                differenceSpreadsheet.ShowDialog();
                if (differenceSpreadsheet.isUpdateRequired==true)
                {
                    MessageBox.Show("Обновление начато. Пожалуйста подождите!");
                    foreach (SpreadsheetLine line in missingLines)
                    {
                        mainSpreadsheet.listOfLines.Add(line);
                    }
                    var SortingLine = from line in mainSpreadsheet.listOfLines
                                                  orderby line.ID
                                                  select line;
                    mainSpreadsheet.listOfLines = new List<SpreadsheetLine>();
                    foreach(var item in SortingLine)
                    {
                        mainSpreadsheet.listOfLines.Add(item);
                    }
                    mainSpreadsheet.currentPage = 0;
                    if (!(mainSpreadsheet.listOfLines.Count == 0) && !(mainSpreadsheet.listOfPages == null))
                    {
                        mainSpreadsheet.SplitForPages();
                        SpreadsheetView.ItemsSource = mainSpreadsheet.listOfPages[mainSpreadsheet.currentPage];
                    }
                    else
                    {
                        SpreadsheetView.ItemsSource = null;
                    }
                    CreateButtonsForPages(mainSpreadsheet);
                    try
                    {
                        List<List<SpreadsheetLine>> difference = Spreadsheet.CheckDifferencesToOnline(mainSpreadsheet);
                        MissingSpreadsheetLines.Text = $"Недостающие записи в локальной копии: {difference[0].Count}";
                        if (difference[0].Count > 0)
                        {
                            ShowMissingLines.IsEnabled = true;
                        }
                        if (difference[1].Count > 0)
                        {
                            ShowChangedLines.IsEnabled = true;
                        }
                        ChangedSpreadsheetLines.Text = $"Изменённые записи в локальной копии: {difference[1].Count}";
                    }
                    catch
                    {
                        MissingSpreadsheetLines.Text = $"Недостающие записи в локальной копии: неизвестно";
                        ChangedSpreadsheetLines.Text = $"Изменённые записи в локальной копии: неизвестно";
                    }
                }
                ShowMissingLines.IsEnabled = false;

            };
           //===========================================================================================
            ShowChangedLines.Click += (sender, args) =>
            {
                List<SpreadsheetLine> changedLines = Spreadsheet.CheckDifferencesToOnline(mainSpreadsheet)[1];
                ShowDifference differenceSpreadsheet = new ShowDifference();
                //differenceSpreadsheet.DifferenceView.ItemsSource = changedLines;
                Dictionary<SpreadsheetLine, string> differences = new Dictionary<SpreadsheetLine, string>();
                foreach (SpreadsheetLine line in changedLines)
                {
                    string differentFields="";
                    foreach (SpreadsheetLine originalLine in mainSpreadsheet.listOfLines)
                    {
                        if (line.ShortToString().Substring(4, 3).Equals(originalLine.ShortToString().Substring(4, 3)))
                        {
                            if (originalLine.Name != line.Name) { differentFields += "Имя\n"; }
                            if (originalLine.Description != line.Description) { differentFields += "Описание\n"; }
                            if (originalLine.SourceOfThreat != line.SourceOfThreat) { differentFields += "Источник угрозы\n"; }
                            if (originalLine.ThreatenObject != line.ThreatenObject) { differentFields += "Объект воздействия\n"; }
                            if (originalLine.ConfidentialityBreach != line.ConfidentialityBreach) { differentFields += "Угроза конфиденциальности \n"; }
                            if (originalLine.IntegrityBreach != line.IntegrityBreach) { differentFields += "Угроза целостности\n"; }
                            if (originalLine.AvailabilityBreach != line.AvailabilityBreach) { differentFields += "Угроза доступности\n"; }
                            if (originalLine.IntroductionDate != line.IntroductionDate) { differentFields += "Дата введения\n"; }
                            if (originalLine.LastChangeDate != line.LastChangeDate) { differentFields += "Дата последнего изменения\n"; }
                        }
                    }
                    differences.Add(line, differentFields);
                }
                differenceSpreadsheet.DifferenceView.ItemsSource = differences;
                differenceSpreadsheet.ShowDialog();
                if (differenceSpreadsheet.isUpdateRequired == true)
                {
                    MessageBox.Show("Обновление начато. Пожалуйста подождите!");
                        foreach (SpreadsheetLine line in changedLines)
                        {
                            foreach (SpreadsheetLine originalLine in mainSpreadsheet.listOfLines)
                            {
                                if (line.ShortToString().Substring(4, 3).Equals(originalLine.ShortToString().Substring(4, 3)))
                                {
                                    mainSpreadsheet.listOfLines.Remove(originalLine);
                                    break;
                                }
                            }
                            mainSpreadsheet.listOfLines.Add(line);
                        }
                    var SortingLine = from line in mainSpreadsheet.listOfLines
                                      orderby line.ID
                                      select line;
                    mainSpreadsheet.listOfLines = new List<SpreadsheetLine>();
                    foreach (var item in SortingLine)
                    {
                        mainSpreadsheet.listOfLines.Add(item);
                    }
                    mainSpreadsheet.currentPage = 0;
                    if (!(mainSpreadsheet.listOfLines.Count == 0) && !(mainSpreadsheet.listOfPages == null))
                    {
                        mainSpreadsheet.SplitForPages();
                        SpreadsheetView.ItemsSource = mainSpreadsheet.listOfPages[mainSpreadsheet.currentPage];
                    }
                    else
                    {
                        SpreadsheetView.ItemsSource = null;
                    }
                    CreateButtonsForPages(mainSpreadsheet);
                    try
                    {
                        List<List<SpreadsheetLine>> difference = Spreadsheet.CheckDifferencesToOnline(mainSpreadsheet);
                        MissingSpreadsheetLines.Text = $"Недостающие записи в локальной копии: {difference[0].Count}";
                        if (difference[0].Count > 0)
                        {
                            ShowMissingLines.IsEnabled = true;
                        }
                        if (difference[1].Count > 0)
                        {
                            ShowChangedLines.IsEnabled = true;
                        }
                        ChangedSpreadsheetLines.Text = $"Изменённые записи в локальной копии: {difference[1].Count}";
                    }
                    catch
                    {
                        MissingSpreadsheetLines.Text = $"Недостающие записи в локальной копии: неизвестно";
                        ChangedSpreadsheetLines.Text = $"Изменённые записи в локальной копии: неизвестно";
                    }
                    ShowChangedLines.IsEnabled = false;
                }

            };

        }

        private void CloseApplication(object sender, System.Windows.RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void ButtonParameters(object sender, System.Windows.RoutedEventArgs e)
        {
            Window configWindow = new Window();
            configWindow.Owner = this;
            configWindow.Show();
            configWindow.Title = "Параметры";
            configWindow.Height = 800;
            configWindow.Width = 450;
            configWindow.ResizeMode = ResizeMode.NoResize;
        }
        private void CreateButtonsForPages(Spreadsheet spreadsheet)
        {
            ButtonsForPages.Children.Clear();
            for (int i = 0; i < spreadsheet.listOfPages.Count; i++)
            {
                System.Windows.Controls.Button pageButton = new System.Windows.Controls.Button();

                pageButton.Content = (i + 1).ToString();
                pageButton.Name = $"PageButton{i + 1}";
                pageButton.Height = 20;
                pageButton.Width = 20;
                pageButton.Click += (sender, args) =>
                {
                    SpreadsheetView.ItemsSource = spreadsheet.listOfPages[Int32.Parse(pageButton.Content.ToString()) - 1];
                    spreadsheet.currentPage = Int32.Parse(pageButton.Content.ToString()) - 1;
                };
                ButtonsForPages.Children.Add(pageButton);
            }
        }
        private Spreadsheet InitializeSpreadsheet()
        {
            Spreadsheet mainSpreadsheet = new Spreadsheet();
            this.Title = $"{Config.filename}";
            ShowMissingLines.IsEnabled = false;
            ShowChangedLines.IsEnabled = false;
            ButtonDownload.Visibility = Visibility.Hidden;
            ButtonFindLocal.Visibility = Visibility.Hidden;
            NotFoundMessage.Visibility = Visibility.Hidden;
            ChangedSpreadsheetLines.Visibility = Visibility.Visible;
            MissingSpreadsheetLines.Visibility = Visibility.Visible;
            ShowChangedLines.Visibility = Visibility.Visible;
            ShowMissingLines.Visibility = Visibility.Visible;
            DateOfLastChange.Text = $"Дата последнего изменения файла: {File.GetLastWriteTime($@"{Config.filepath}\{Config.filename}").ToString("dd.MM.yyyy - HH:mm")}";
            DateOfLastChange.Visibility = Visibility.Visible;
            mainSpreadsheet.ReadSpreadsheet();
            ShowNumberOFElements.Text = $"Количество записей в файле:  {mainSpreadsheet.listOfLines.Count}";
            ShowNumberOFElements.Visibility = Visibility.Visible;
            mainSpreadsheet.currentPage = 0;
            if (!(mainSpreadsheet.listOfLines.Count==0) && !(mainSpreadsheet.listOfPages ==null))
            {
                SpreadsheetView.ItemsSource = mainSpreadsheet.listOfPages[mainSpreadsheet.currentPage];
            }
            else
            {
                SpreadsheetView.ItemsSource = null;
            }
            CreateButtonsForPages(mainSpreadsheet);
            try
            {
                List<List<SpreadsheetLine>> difference = Spreadsheet.CheckDifferencesToOnline(mainSpreadsheet);
                MissingSpreadsheetLines.Text = $"Недостающие записи в локальной копии: {difference[0].Count}";
                if (difference[0].Count>0)
                {
                    ShowMissingLines.IsEnabled = true;
                }
                if (difference[1].Count>0)
                {
                    ShowChangedLines.IsEnabled = true;
                }
                ChangedSpreadsheetLines.Text = $"Изменённые записи в локальной копии: {difference[1].Count}";
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удаётся сравнить локальную копию с последней версией файла: "+ e.Message);
                MissingSpreadsheetLines.Text = $"Недостающие записи в локальной копии: неизвестно";
                ChangedSpreadsheetLines.Text = $"Изменённые записи в локальной копии: неизвестно";
            }
            return mainSpreadsheet;
        }
 
    }
}
