﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;

namespace Spreadsheet_Viewer
{

    public partial class FileExplorer : Window
    {
        static public string currentName;
        static public string currentFolder;
        static private bool showAllFiles = true;
        static private bool isInitializedDisks;
        static public bool isContinueAllowed;
        static public bool isForSave=false;
        public FileExplorer()
        {
            currentFolder = Config.filepath;
            isInitializedDisks = false;
            isContinueAllowed = false;
            isForSave = false;
            InitializeComponent();
            ReadDisks();
            ReadFoldersInFolder($@"{currentFolder}");
            ReadFilesInFolder($@"{currentFolder}");
        }
        private string[] ReadDisks()
        {
            string[] disksOnPC = Environment.GetLogicalDrives();
            int setCurrentDisk = 0;
            for (int i=0; i<disksOnPC.Length; i++)
            {
                if (disksOnPC[i]==currentFolder.Substring(0,3))
                {
                    setCurrentDisk = i;
                }
                ListOfDisks.Items.Add(disksOnPC[i]);
            }
            if (!isInitializedDisks)
            {
                ListOfDisks.SelectedIndex = setCurrentDisk;
                currentFolder = Config.filepath;
            }
            isInitializedDisks = true;
            return disksOnPC;
        }
        private string[] ReadFoldersInFolder(string folder)
        {
            FoldersList.Children.Clear();
            FieldForCurrentPath.Content = ShortenPathToFile(currentFolder);
            string[] foldersInFolder = Directory.GetDirectories($@"{folder}");
            for (int i=0; i<foldersInFolder.Length; i++)
            {
                Button folderButton = new Button();
                folderButton.Content = foldersInFolder[i].Substring(foldersInFolder[i].LastIndexOf(@"\") + 1, foldersInFolder[i].Length - foldersInFolder[i].LastIndexOf(@"\") - 1);
                folderButton.HorizontalContentAlignment = HorizontalAlignment.Left;
                folderButton.BorderBrush = Brushes.Transparent;
                folderButton.Name = $"folderButton{i}";
                folderButton.Height = 20;
                folderButton.Background = Brushes.Transparent;
                folderButton.Click += (sender, args) =>
                {
                    currentFolder = currentFolder + $@"\{folderButton.Content.ToString()}";
                    ReadFoldersInFolder($@"{currentFolder}");
                    ReadFilesInFolder($@"{currentFolder}");
                };
                FoldersList.Children.Add(folderButton);

            }
            return foldersInFolder;
        }

        private List<string> ReadFilesInFolder(string folder)
        {
            FilesList.Children.Clear();
            string[] filesInFolderInput = Directory.GetFiles($@"{folder}");
            List<string> filesInFolder = new List<string>();
            foreach (string filename in filesInFolderInput)
            {
                if (showAllFiles == false)
                {
                    filesInFolder.Add(filename);
                }
                if (showAllFiles == true && filename.EndsWith(".xlsx"))
                {
                    filesInFolder.Add(filename);
                }
            }
            for (int i = 0; i<filesInFolder.Count; i++)
            {
                Button fileButton = new Button();
                fileButton.Content = filesInFolder[i].Substring(filesInFolder[i].LastIndexOf(@"\")+1,filesInFolder[i].Length-filesInFolder[i].LastIndexOf(@"\")-1);
                fileButton.HorizontalContentAlignment = HorizontalAlignment.Left;
                fileButton.BorderBrush = Brushes.Transparent;
                fileButton.Name = $"fileButton{i}";
                fileButton.Height = 20;
                fileButton.Background = Brushes.Transparent;
                fileButton.Click += (sender, args) =>
                {
                    TextBox_FileName.Text = fileButton.Content.ToString();
                };
                FilesList.Children.Add(fileButton);
            }
            return filesInFolder;
        }

        private void UpOneFolder_Click(object sender, RoutedEventArgs e)
        {
            if (currentFolder.Length > 3)
            {
                currentFolder = currentFolder.Substring(0, currentFolder.LastIndexOf(@"\"));
                FilesList.Children.Clear();
                FoldersList.Children.Clear();
                ReadFilesInFolder($@"{currentFolder}");
                ReadFoldersInFolder($@"{currentFolder}");
            }
        }

        private void ShowAllFiles_Checked(object sender, RoutedEventArgs e)
        {
            showAllFiles = true;
            ReadFilesInFolder(currentFolder);
        }

        private void ShowAllFiles_Unchecked(object sender, RoutedEventArgs e)
        {
            showAllFiles = false;
            ReadFilesInFolder(currentFolder);
        }

        private void DisksBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            try
            {
                currentFolder = comboBox.SelectedItem.ToString();
                ReadFoldersInFolder($@"{currentFolder}");
                ReadFilesInFolder($@"{currentFolder}");
            }
            catch (Exception exep)
            {
                MessageBox.Show("Не удалось получить доступ к диску: " + exep.Message);
                string[] disksOnPC = Environment.GetLogicalDrives();
                int setCurrentDisk = 0;
                currentFolder = Config.filepath;
                for (int i = 0; i < disksOnPC.Length; i++)
                {
                    if (disksOnPC[i] == currentFolder.Substring(0, 3))
                    {
                        setCurrentDisk = i;
                    }
                }
                ListOfDisks.SelectedIndex = setCurrentDisk;
                currentFolder = Config.filepath;
                ReadFoldersInFolder($@"{currentFolder}");
                ReadFilesInFolder($@"{currentFolder}");
            }
        }

        private void DefaultPath_Click(object sender, RoutedEventArgs e)
        {
            currentFolder= Assembly.GetExecutingAssembly().Location.Substring(0, Assembly.GetExecutingAssembly().Location.LastIndexOf(@"\"));
            ReadFoldersInFolder($@"{currentFolder}");
            ReadFilesInFolder($@"{currentFolder}");
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (!TextBox_FileName.IsEnabled || isForSave || NameIsCorrect())
            {
                currentName = TextBox_FileName.Text;
                Config.filepath = currentFolder;
                Config.filename = currentName;
                isContinueAllowed = true;
                this.Close();
            }
        }
        private bool NameIsCorrect()
        {
            string ForbiddenSymbols = @"~#%&*{}\:<>?/+|'";
            if (TextBox_FileName.Text.Length == 0)
            {
                MessageBox.Show("Имя файла не может быть пустым");
                return false;
            }
            else
            {
                foreach (char letter in TextBox_FileName.Text)
                {
                    foreach (char symbol in ForbiddenSymbols)
                    {
                        if (letter == symbol)
                        {
                            MessageBox.Show($"Имя файла не может содержать символ {symbol}");
                            return false;
                        }
                    }
                }
            }
            string[] filesInFolderInput = Directory.GetFiles($@"{currentFolder}");
            List<string> filesInFolder = new List<string>();
            foreach (string filename in filesInFolderInput)
            {
                if (filename.EndsWith(".xlsx"))
                {
                    filesInFolder.Add(filename.Substring(filename.LastIndexOf(@"\")+1));
                }
            }
            if (TextBox_FileName.Text.Substring(TextBox_FileName.Text.Length - 5, 5) != ".xlsx")
            {
                TextBox_FileName.Text = TextBox_FileName.Text + ".xlsx";
            }
            if (!filesInFolder.Contains(TextBox_FileName.Text))
            {
                MessageBox.Show("Файла с таким именем не существует в этой папке");
                return false;
            }
            return true;
        }
        public static string ShortenPathToFile(string pathToFile)
        {
            if (pathToFile.Length > 55)
            {
                int lastAvailableIndex = pathToFile.IndexOf(@"\", pathToFile.Length - 55);
                return "..." + pathToFile.Substring(lastAvailableIndex, pathToFile.Length - lastAvailableIndex);
            }
            else { return pathToFile; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            isContinueAllowed = false;
            this.Close();
        }
    }
}
