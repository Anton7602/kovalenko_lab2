﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using OfficeOpenXml;

namespace Spreadsheet_Viewer
{
    class SpreadsheetLine
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SourceOfThreat { get; set; }
        public string ThreatenObject { get; set; }
        public bool ConfidentialityBreach { get; set; }
        public bool IntegrityBreach { get; set; }
        public bool AvailabilityBreach { get; set; }
        public string IntroductionDate { get; set; }
        public string LastChangeDate { get; set; }
        public static List<string[]> ReadTitles()
        {
            List<string[]> spreadsheetHeaderslocal = new List<string[]>();
            byte[] bin = File.ReadAllBytes($@"{Config.filepath}\{Config.filename}");
            try
            {
                using (MemoryStream stream = new MemoryStream(bin))
                using (ExcelPackage excelPackage = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[1];
                    string[] line = new string[3] { $"{worksheet.Cells["A1"].Value.ToString()}",$"{worksheet.Cells["F1"].Value.ToString()}",$"{worksheet.Cells["I1"].Value.ToString()}" };
                    spreadsheetHeaderslocal.Add(line);
                    line = new string[10];
                    string alphabet = "ABCDEFGHIJ";
                    for (int i=0; i<alphabet.Length; i++)
                    {
                        line[i] = $"{worksheet.Cells[$"{alphabet[i]}2"].Value.ToString()}";
                    }
                    spreadsheetHeaderslocal.Add(line);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка при считывании строки: " + e.Message);
            }
            Spreadsheet.SpreadsheetHeaders = spreadsheetHeaderslocal;
            return spreadsheetHeaderslocal;
        }
        public static SpreadsheetLine ReadLine(Spreadsheet spreadsheet)
        {
            spreadsheet.lastReadRow++;
            SpreadsheetLine line = new SpreadsheetLine();
                byte[] bin = File.ReadAllBytes($@"{Config.filepath}\{Config.filename}");
                try
                {
                    using (MemoryStream stream = new MemoryStream(bin))
                    using (ExcelPackage excelPackage = new ExcelPackage(stream))
                    {
                        ExcelWorksheet currentWorksheet = excelPackage.Workbook.Worksheets[1];
                        line.ID = Int32.Parse(currentWorksheet.Cells[spreadsheet.lastReadRow, 1].Value.ToString());
                        line.Name = currentWorksheet.Cells[spreadsheet.lastReadRow, 2].Value.ToString();
                        line.Description = currentWorksheet.Cells[spreadsheet.lastReadRow, 3].Value.ToString();
                        line.Description = line.Description.Replace("_x000d_", "");
                        line.SourceOfThreat = currentWorksheet.Cells[spreadsheet.lastReadRow, 4].Value.ToString();
                        line.ThreatenObject = currentWorksheet.Cells[spreadsheet.lastReadRow, 5].Value.ToString();
                        line.ConfidentialityBreach = (Int32.Parse(currentWorksheet.Cells[spreadsheet.lastReadRow, 6].Value.ToString()) == 1);
                        line.IntegrityBreach = (Int32.Parse(currentWorksheet.Cells[spreadsheet.lastReadRow, 7].Value.ToString()) == 1);
                        line.AvailabilityBreach = (Int32.Parse(currentWorksheet.Cells[spreadsheet.lastReadRow, 8].Value.ToString()) == 1);
                        try
                        {
                            line.IntroductionDate = DateTime.FromOADate(long.Parse(currentWorksheet.Cells[spreadsheet.lastReadRow, 9].Value.ToString())).ToString("dd.MM.yyyy");
                            line.LastChangeDate = DateTime.FromOADate(long.Parse(currentWorksheet.Cells[spreadsheet.lastReadRow, 10].Value.ToString())).ToString("dd.MM.yyyy");
                        }
                        catch
                        {
                            line.IntroductionDate = currentWorksheet.Cells[spreadsheet.lastReadRow, 9].Value.ToString();
                            line.LastChangeDate = currentWorksheet.Cells[spreadsheet.lastReadRow, 10].Value.ToString();
                        }
                }
                spreadsheet.listOfLines.Add(line);
                    
                }
                catch (Exception e)
                {
                    MessageBox.Show("Ошибка при считывании строки: " + e.Message);
                }
            return line;
        }
        public override string ToString()
        {
            if (this.Description.Length != 0 && this.Description != null)
            {
                if (this.Description.Length > 24)
                {
                    return $"{this.ID}: {this.Name} - {this.Description.Substring(0, 25)}..." +
                        $"\nИсточник: {this.SourceOfThreat}; Объект угрозы: {this.ThreatenObject} " +
                        $"\nНарушения: Конфиденциальности ({this.ConfidentialityBreach}); Целостности ({this.IntegrityBreach}); Доступности ({this.AvailabilityBreach}) " +
                        $"\nДата внесения: {IntroductionDate.ToString()}; Дата последнего изменения: {this.LastChangeDate.ToString()}";
                }
                else
                {
                    return $"{this.ID}: {this.Name} - {this.Description}" +
                        $"\nИсточник: {this.SourceOfThreat}; Объект угрозы: {this.ThreatenObject} " +
                        $"\nНарушения: Конфиденциальности ({this.ConfidentialityBreach}); Целостности ({this.IntegrityBreach}); Доступности ({this.AvailabilityBreach}) " +
                        $"\nДата внесения: {IntroductionDate.ToString()}; Дата последнего изменения: {this.LastChangeDate.ToString()}";
                }
            }
            else
            {
                return $"{this.ID}: {this.Name}" +
                    $"\nИсточник: {this.SourceOfThreat}; Объект угрозы: {this.ThreatenObject} " +
                    $"\nНарушения: Конфиденциальности ({this.ConfidentialityBreach}); Целостности ({this.IntegrityBreach}); Доступности ({this.AvailabilityBreach}) " +
                    $"\nДата внесения: {IntroductionDate.ToString()}; Дата последнего изменения: {this.LastChangeDate.ToString()}";
            }
        }
        public string ShortToString()
        {
            string longID = this.ID.ToString();
            while(longID.Length<3)
            {
                longID = "0" + longID;
            }
            longID = "УБИ." + longID;
            return $@"{longID} - {this.Name}";
        }
    }
}
