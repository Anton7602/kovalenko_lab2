﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;
using System.IO;

namespace Spreadsheet_Viewer
{
    abstract class Config
    {
        public static string filepath;
        public static string fileurl;
        public static string filename;

        public static void OverrideConfigFile()
        {
            using (StreamWriter configOverride = new StreamWriter($@"Config.ini", false))
            {
                configOverride.WriteLine($"[Last_used_filepath]\n{Config.filepath}");
                configOverride.WriteLine($"[Last_used_fileurl]\n{Config.fileurl}");
                configOverride.WriteLine($"[Last_used_filename]\n{Config.filename}");
            }
        }
        public static void InitiateConfigFile()
        {
            StreamReader configRead = new StreamReader("Config.ini");
            if (File.Exists("Config.ini"))
            {
                List<string> lines = new List<string>();
                do
                {
                    lines.Add(configRead.ReadLine());
                }
                while (lines.Last() != null);
                lines.RemoveAt(lines.Count-1);
                for (int i=0; i<lines.Count; i++)
                {
                    if (lines[i].Contains("[Last_used_filepath]"))
                    {
                        filepath = lines[i + 1];
                    }
                    if (lines[i].Contains("[Last_used_fileurl]"))
                    {
                        fileurl = lines[i + 1];
                    }
                    if (lines[i].Contains("[Last_used_filename]"))
                    {
                        filename = lines[i + 1];
                    }
                }
            }
            else 
            {
                filepath = Assembly.GetExecutingAssembly().Location.Substring(0, Assembly.GetExecutingAssembly().Location.LastIndexOf(@"\"));
                fileurl = $@"https://bdu.fstec.ru/documents/files/thrlist.xlsx";
                filename = "thrlist.xlsx";
            }
            configRead.Close();
        }
    }
}
